<?php

use Illuminate\Database\Seeder;
use \App\Post;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 30;
        $posts = Post::all();

        for ($i = 0; $i < $limit; $i++) {
            DB::table('comments')->insert([ //,
                'body' => $faker->realText(150) ,
                'post_id' => $posts->random()->id
            ]);
        }
    }
}
