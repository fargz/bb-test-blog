<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['General', 'Technology', 'White Papers', 'Culture', 'Reviews'];
        foreach($categories as $key=>$value){
            DB::table('categories')->insert([ //,
                'name' => $value
            ]);
        }
    }
}
