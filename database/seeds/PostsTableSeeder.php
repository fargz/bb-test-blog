<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 50;
        $users = \App\User::all();

        for ($i = 0; $i < $limit; $i++) {
            DB::table('posts')->insert([ //,
                'title' => $faker->sentence,
                'body' => $faker->paragraph ,
                'user_id' => $users->random()->id,
                'image' => 'posts/Ro9qdaa812fqq7jBDTuPnwD6ZIjATOBoK3yHBSdW.jpeg',
                'published_at' => $faker->dateTimeThisYear(),
                'created_at' => $faker->dateTimeThisYear()
            ]);
        }
    }
}
