<?php

use Illuminate\Database\Seeder;
use \App\Post;

class PostCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;
        $posts = Post::all();
        $categories = \App\Category::all();
        foreach($posts as $post){
            DB::table('posts_categories')->insert([ //,
                'post_id' => $post->id,
                'category_id' => 1
            ]);

            // Check if this key => value pair already exists.  if so, skip this iteration
            $random_cat_id =   $faker->numberBetween($min = 2, $max = 5);
            if (!array_key_exists($random_cat_id, $post->categories()->pluck('category_id')->toArray())){
            DB::table('posts_categories')->insert([ //,
                'post_id' => $post->id,
                'category_id' => $random_cat_id
            ]);
        }

           
        }

        /*for ($i = 0; $i < $limit; $i++) {
            DB::table('posts_categories')->insert([ //,
                'post_id' => $posts->random()->id,
                'category_id' => $faker->numberBetween($min = 2, $max = 5)
            ]);
        }*/
    }
}
