<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
  @if(isset($show_search) && $show_search)
          <div class="sidebar-module sidebar-module-inset">
            <h4>Search</h4>
            <p>
              <form action="/search" role="search">
                  <div class="input-group">
                      <input type="text" class="form-control" name="search" placeholder="Search for..." value="{{ app('request')->input('search')}} ">
                      <span class="input-group-btn">
                        <button class="btn btn-success" type="submit">Go!</button>
                      </span>
                    </div><!-- /input-group -->
              </form>
            </p>
          </div>
          @endif
          <div class="sidebar-module">
            
            @if(isset($categories) && count($categories))
            <h4>Categories</h4>
            <ol class="list-unstyled">
              @foreach($categories as $category)
              <li><a href="#">{{$category->name}}</a></li>
              @endforeach
            </ol>
            @endif
          </div>
        </div><!-- /.blog-sidebar -->