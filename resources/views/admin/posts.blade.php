@extends('layouts.master')

@section('content')
<div class="col-sm-12">
        @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <div class="alert alert-success hidden alertMsg"></div>

        @if(!$posts->isEmpty())
    
    
    <div class="table-responsive">
<table class="table table-striped"> 
    <thead> 
        <tr> 
            <th width="20%">Title</th> 
            <th width="50%">Description</th> 
            <th width="10%">Categories</th> 
            <th width="15%">Publish Date</th> 
            <th width="5%">Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            @foreach($posts as $post)
            <tr> 
                <td><a href="posts/{{$post->id}}/edit/">{{$post->title}}</a></td> 
                <td>{{$post->body}}</td> 
                <td>
                    @foreach($post->categories as $post_cat)
                    {{$post_cat->name}},
                    @endforeach
                </td> 
                <td>{{$post->published_at->toFormattedDateString()}}</td> 
                <td>
                    <a class="" href="{{ URL::to('posts/' . $post->id . '/edit') }}"><span class="text-warning glyphicon glyphicon-edit"></a>&nbsp;
                    <a class="deletePost" data-id="{{$post->id}}"  href="#"><span class="text-danger glyphicon glyphicon-trash"></a>
                </td> 
            </tr> 
            @endforeach() 
        </tbody> 
    </table>
     <div class="text-center">{{ $posts->links() }}</div>
    @else
    <div class="alert alert-warning" role="alert"> <strong>Oh snap!</strong> No posts added yet!.
        <p><a class="btn btn-primary" href="{{url('posts/create')}}">Add Post</a></p> 
    </div>
    @endif  
</div>
</div>
    @endsection

    @section('script')
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.deletePost').on('click', function(e){
                        e.preventDefault();
                        var id =  $(this).attr('data-id');
                        var parent = $(this).parent();
                        var _token = "{{ csrf_token() }}";
                        parent.closest("tr").css({'background':'red'});
                        $.ajax({
                            url     : "{{url('/posts/delete')}}",
                            type    : 'post',
                            dataType   : 'json',
                            data    :   {
                                            'id': id,
                                            '_token':_token
                                        },
                            beforeSend : function() {
                
                            },
                            complete   : function(resp) {
                                //alert(resp);
                                if(resp.success){
                                    parent.closest("tr").slideUp(400, function () {
                                        parent.closest("tr").remove();
                                        $('.alertMsg').text('Post deleted successfully').toggleClass('hidden');
                                        setInterval(function() {
                                            $('.alertMsg').fadeOut('slow').toggleClass('hidden');
                                        }, 5000);
                                    });
                                }
                            },
                            success    : function(resp) 
                            {
                             console.log(resp);
                            }
                        });
                    
                });
                });
                
                </script>
    @endsection