@extends('layouts.master')

@section('content')
<div class="col-sm-8 blog-main">
  @if(isset($posts))
        @foreach($posts as $post)
         @include('posts.post')
         @endforeach
         {{ $posts->links() }}
         @endif
         @if(isset($msg))
         {{ $msg }}
         @endif
</div><!-- /.blog-main -->
@endsection

@section('footer')


@endsection