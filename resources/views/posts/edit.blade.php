@extends('layouts.master')

@section('content')

<div class="col-sm-8 blog-main">
    <h3>Edit a Post</h3>
    <hr/>
    @include('partials.errors')
    <form method="POST" action="/posts/{{$post->id}}/edit" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" class="form-control" id="post_title" name="title" value="{{ $post->title }}">
        </div>
        <div class="form-group">
            <label for="comment">Post:</label>
            <textarea class="form-control" rows="5" id="body" name="body" value="{{ $post->body }}">{{ $post->body}}</textarea>
        </div>
        <div class="form-group">
            <label for="comment">Featured Image:</label>
            <input type="file" id="post_image" name="image" >
        </div>

        <div class="form-group">
            <label for="published_at">Publish Date:</label>
            <div class='input-group' id='datetimepicker1'>
                <input type='text' class="form-control date" name="published_at" value="{{ $post->published_at->format('m/d/Y')}}" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <div class="form-group">
            <label for="categories">Categories:</label>
            @foreach($categories as $cat)
            <div class="checkbox">
                <label>{{ Form::checkbox('category[]', $cat->id, in_array($cat->id, $checked_cat)) }}{{$cat->name}}</label>
            </div>
            @endforeach
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection

@section('script')

@endsection