@extends('layouts.master')

@section('content')
<div class="col-sm-8 blog-main">
    <div class="blog-post">
            <h3 class="text-primary">{{$post->title}}</h3>
            
            <p class="blog-post-meta">{{ $post->created_at->toFormattedDateString()}} by <a href="#">{{$post->user->name}}</a></p>
            <p><img src="/posts/image/{{$post->id}}"  class="img-responsive"  alt="post image"></p>
            <p>{{ $post->body}}</p>
            
            
            <p>
                @foreach($post_categories as $post_cat)
                <span class="label label-default">{{$post_cat->name}}</span>
                @endforeach
            </p>
    </div>
    <div class="comment">
        <form method="POST" action="/posts/{{$post->id}}/comments/">
            {{csrf_field()}}
            <div class="form-group">
                <label for="exampleInputEmail1">Comment:</label>
                <input type="text" class="form-control" name="post_comment" id="comment" required placeholder="Leave a comment">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Add Comment">
            </div>
        </form>
    </div>
    <div>
        @foreach($post->comments as $comment)
        <blockquote>
            <p>{{$comment->body}}</p>
            <footer>Anonymous</footer>
        </blockquote>
        @endforeach
    </div>  
</div>
@endsection
