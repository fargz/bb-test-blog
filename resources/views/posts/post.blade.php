<div class="blog-post">
        <h3><a href="posts/{{$post->id}}">{{$post->title}}</a></h3>
        
        <p class="blog-post-meta">{{ $post->published_at->toFormattedDateString()}} by <a href="#">{{$post->user->name}}</a></p>
        <p><img src="/posts/image/{{$post->id}}"  class="img-responsive"  alt="post image"></p>
        @if(isset($listing) && $listing)
            {{  str_limit($post->body, 100, '...') }}
            <a href="/posts/{{$post->id}}"> Continue Reading More</a>
        @else
        <p>{{ $post->body}}</p>
        @endif
        <hr>
        <p>Categories: 
            @foreach($post->categories as $post_cat)
            <span class="label label-default">{{$post_cat->name}}</span>
            @endforeach
        </p>
</div>
