<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PostsController@index');

Route::any('/search', 'PostsController@search');






Route::group(['middleware' =>'auth'], function () {

    Route::get('/posts/create', 'PostsController@create');
    Route::post('/posts', 'PostsController@store');
    Route::post('/posts/delete', 'PostsController@delete');
    Route::get('/posts/{id}/edit', 'PostsController@edit');
    Route::post('/posts/{id}/edit', 'PostsController@update');
});

Route::get('/posts/{id}', 'PostsController@show');
Route::post('/posts/{post}/comments', 'CommentsController@store');

Route::get('/posts/image/{id}', function($id){
    $post = \App\Post::find($id);
    $path = storage_path('app') .'/'. $post->image;
        
    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');
