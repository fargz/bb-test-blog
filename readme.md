##Pre-requisites ##
Make sure you have a composer and a web server installed on your machine.\

##Installation ##

Extract and copy the project folder

Create a new database and place the details at .env file in laravel project folder DB_DATABASE= DB_USERNAME= DB_PASSWORD=

Navigate terminal/cmd and run the following commands.

1. composer install 
2. php artisan key:generate 
3. php artisan cache:clear 
4. php artisan migrate --seed 
5. php artisan serve