<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Auth;
use File;
use Session;
use Carbon\Carbon;

class PostsController extends Controller
{
    //
    public function __construct()
    {
        $this->showSideBar = true;
        $this->categories = Category::all();
    }

    public function index(){
        $posts = Post::publishlatest()->paginate('10');
        //print_r(\DB::getQueryLog());
        return view('posts.index')->with([
            'posts' => $posts,
            'listing' => true,
            'categories' =>$this->categories, 
            'show_search' => true
        ]);
    }

    public function create(){
        
        return view('posts.create')->with([
            'categories' =>$this->categories,
            'checked_cat' => array('1'),
            ]);
    }

    public function edit($id)
    {
        //
        
        $post =  Post::find($id);

        if (Auth::user()->id !== $post->user_id)
        {
            return redirect('dashboard')->withError("Un-Authorise access");
        }
        $checked_cat = $post->categories->pluck('id')->toArray();
        return view('posts.edit')->with([
            'categories' => $this->categories,
            'checked_cat' => $checked_cat,
            'post' => $post
            ]);
    }

    public function store(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('post/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        $published_at = Carbon::createFromFormat('m/d/Y', $request->published_at);
        $post = Post::create([
            'title' => $request->title,
            'body' => $request->body,
            'user_id' => $userId,
            'published_at' => $published_at,
            'image' => ''
        ]);

        $post->categories()->attach($request->category);

       // $post_image_filename = $request->image->store('posts');
        $filename = $request->image->store('posts');
        //update filename
        $post->image = $filename;
        $post->save();

        Session::flash('flash_message', 'Post successfully added!');
        return redirect('dashboard');

    }  
    
    public function update($id, Request $request)
    {
        // validate
        $rules = array(
            'title' => 'required|max:255',
            'body' => 'required',
            'image' => 'image'
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return redirect('posts/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $post = Post::find($id);
            $post->title      = $request->title;
            $post->body      = $request->body;
            $post->published_at      =  Carbon::createFromFormat('m/d/Y', $request->published_at);
            //$post->save();
            //delete old categories
            //$post->categories()->delete();
            //return $post;
            //$post->categories()->detach($request->category);
            $post->categories()->sync($request->category);

            if($request->hasFile('image')){
                $filename = $request->image->store('posts');
                //update filename
                $post->image = $filename;
            }
            
            $post->save();

            Session::flash('message', 'Successfully updated post!');
            return redirect('dashboard');
        }
    }

    public function show($id)
    {
        //
        $post =  Post::findorFail($id);
        $post_categories = $post->categories;
        return view('posts.single')->with([
            'post' => $post,
            'categories' =>$this->categories,
            'post_categories' =>$post_categories,
            'show_search' => true
        ]);
    }

    public function get_image($id)
    {
        $post = Post::find($id);

        $path = storage_path() .'/posts/'. $post->image;
        
        if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
    
    //return Image::make(storage_path() .'/posts/' .'Y35Ey3yYhAz9f3Jl8USkQfTIjeYyHYGAaajeIloL.jpeg')->response();
    }

    public function search(Request $request){
        $query = $request->search;
 
        $result_posts = Post::where('title','like','%'.$query.'%')->orWhere('body', 'like', '%'.$query.'%')
            ->publishlatest()
            ->paginate(10);
            //dd(DB::getQueryLog());
        if(count($result_posts)){
            return view('posts.index')->with([
                'posts' => $result_posts,
                'listing' => true,
                'categories' => $this->categories
            ]);
        }
        else{
            return view('posts.index')->with([
                'msg' => 'No posts found with the search term == '.$query .'.',
                'categories' => $this->categories
            ]);
        }
        
    }

    public function delete(Request $request){
        if(isset($request->id)){
            $delete_post = Post::findOrFail($request->id);
            $delete_post->delete();

            //delete from relationship table

            return response()->json(['success' => true],200);
      }
    }

}
