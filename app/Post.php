<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    //
    protected $guarded = [];
    protected $dates = ['published_at'];
    //protected $dateFormat = 'U';

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function categories()
    {
       return $this->belongsToMany(Category::class, 'posts_categories');
    }

    public function user()
    {
       return $this->belongsTo(User::class);
    }

    public function scopePublishLatest($query)
    {
        return $query->where('published_at', '<=',  Carbon::now())->orderBy('published_at', 'desc');
    }

}
